// C++ implementation of Shell Sort
#include <iostream>
using namespace std;


typedef struct nodo{
    int dato;
    nodo *sgte;
} *Tnodo;


Tnodo nodo_por_pos(Tnodo lista, int pos ){
    Tnodo p = lista;
    int n = 1;
    while(pos != n){
        p = p->sgte;
        n++;
    }
    return p;
}
Tnodo nodo_por_ind(Tnodo lista, int pos ){
    Tnodo p = lista;
    int n = 0;
    while(pos != n){
        p = p->sgte;
        n++;
    }
    return p;
}

void insertar(Tnodo lista, int data){
    Tnodo nuevo =  NULL;
    nuevo =  new nodo;
    nuevo->dato = data;
    nuevo->sgte = NULL;
    Tnodo p = lista;
    if(lista->dato == 0){
        lista = nuevo;
    }
    while(p->sgte!=NULL){
        p = p->sgte;
    }
    p->sgte = nuevo;
}


int shellSort(Tnodo &lista , int n)
{

	for (int gap = n/2; gap > 0; gap /= 2)
	{

		for (int i = gap; i < n; i += 1)
		{

			int temp = nodo_por_ind(lista,i)->dato;

			int j;		 
			for (j = i; j >= gap && nodo_por_ind(lista,j - gap)->dato > temp; j -= gap)
				nodo_por_ind(lista,j)->dato = nodo_por_ind(lista,j - gap)->dato;
			
			nodo_por_ind(lista,j)->dato= temp;
		}
	}
	return 0;
}

void imprimirlista(Tnodo &lista, int n)
{
	for (int i=0; i<n; i++)
		cout << nodo_por_ind(lista,i)->dato << " ";
}

int main()
{

	int m=0;
	int n=0;
	Tnodo lista = NULL;
    lista =  new nodo;
    lista->dato = 0;
    lista->sgte = NULL;
    
    cout << "CON 0 TERMINA" <<endl;
	do{
		cout << "ESCRIBA" <<endl;
		cin >> m;
		
		insertar(lista, m);
			
		n++;
	
	}while(m!=0);
	
	shellSort(lista, n);
	imprimirlista(lista, n);
	
	
	

	return 0;
}
