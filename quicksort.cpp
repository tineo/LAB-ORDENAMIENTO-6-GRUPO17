#include <iostream>
#include <cstdio>
using namespace std;

struct nodo
{
    int data;
    struct nodo *sgte;
};

void push(struct nodo** ref_cabecera, int n_dato)
{
    struct nodo* new_nodo = new nodo;
    new_nodo->data  = n_dato;
    new_nodo->sgte = (*ref_cabecera);
    (*ref_cabecera)    = new_nodo;
}

void printLista(struct nodo *nodo)
{
    while (nodo != NULL)
    {
       	cout<<nodo->data<<"\t";
        nodo = nodo->sgte;
    }
   cout<<endl;
}

struct nodo *getUlt(struct nodo *cur)
{
    while (cur != NULL && cur->sgte != NULL)
        cur = cur->sgte;
    return cur;
}

struct nodo *particionar(struct nodo *head, struct nodo *end,
                       struct nodo **nuevaCabe, struct nodo **nuevoFin)
{
    struct nodo *pivot = end;
    struct nodo *prev = NULL, *cur = head, *final = pivot;

    while (cur != pivot)
    {
        if (cur->data < pivot->data)
        {

            if ((*nuevaCabe) == NULL)
                (*nuevaCabe) = cur;

            prev = cur;  
            cur = cur->sgte;
        }
        else 
        {

            if (prev)
                prev->sgte = cur->sgte;
            struct nodo *tmp = cur->sgte;
            cur->sgte = NULL;
            final->sgte = cur;
            final = cur;
            cur = tmp;
        }
    }


    if ((*nuevaCabe) == NULL)
        (*nuevaCabe) = pivot;

    (*nuevoFin) = final;


    return pivot;
}

struct nodo *quickSortRecurisvo(struct nodo *inicio, struct nodo *fin)
{

    if (!inicio || inicio == fin)
        return inicio;

    nodo *nuevaCabe = NULL, *nuevoFin = NULL;

    struct nodo *pivot = particionar(inicio, fin, &nuevaCabe, &nuevoFin);


    if (nuevaCabe != pivot)
    {
 
        struct nodo *tmp = nuevaCabe;
        while (tmp->sgte != pivot)
            tmp = tmp->sgte;
        tmp->sgte = NULL;

        nuevaCabe = quickSortRecurisvo(nuevaCabe, tmp);

        tmp = getUlt(nuevaCabe);
        tmp->sgte =  pivot;
    }

    pivot->sgte = quickSortRecurisvo(pivot->sgte, nuevoFin);

    return nuevaCabe;
}

void quickSort(struct nodo **RefInicio)
{
    (*RefInicio) = quickSortRecurisvo(*RefInicio, getUlt(*RefInicio));
    return;
}

int main()
{
    struct nodo *a = NULL;
    int n=0;
    int m=0;
        
    cout << "CON 0 TERMINA" <<endl;
	do{
		cout << "ESCRIBA" <<endl;
		cin >> m;
		
		push(&a,m);
			
		n++;
	
	}while(m!=0);
	


    quickSort(&a);

    printLista(a);

    return 0;
}
